# ecommerceappwithvue

## Figma design link: https://www.figma.com/file/8vC0dCG7ak0SGC4jVYRN8M/AmazonDemo?type=design&node-id=28%3A229&t=bnK1jyR6QKBNa1og-1

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
